<?php 
	class User extends MY_Controller {

		function index() {
			$look_user = $this->uri->segment(3);
			$data = $this->get_data();
			$data['popular'] = $this->ml->m_best_mark($look_user); // sort by DESC
			$data['last_comments'] = $this->ml->m_last_comments($look_user);	// sort by date
			$data['user'] = $this->ml->m_get_user($look_user)

			$data['main_content']='v_profile';
			$this->load->view('includes/template', $data);
		}	
	}