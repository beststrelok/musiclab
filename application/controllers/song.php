<?php 
	class Song extends MY_Controller {

		function __construct() {
			parent::__construct();
			/*------------------------------------------------
			| Checking for login
			------------------------------------------------*/
			// $is_logged_in = $this->session->userdata('is_logged_in');
			// if ($is_logged_in !== 'YES') {
			// 	redirect('welcome');
			// } else {
			// 	$this->current_user_id = $this->session->userdata('user_id');
			// }
			$this->song_id = $this->uri->segment(3);
		}

		function index() {
			$data = $this->get_data();
			// $song_id = $this->uri->segment(3);
			$data['song'] = $this->ml->m_get_songs($this->song_id);
			$data['comments'] = $this->ml->m_get_comments($this->song_id);

			$data['main_content'] = 'v_song';
			$this->load->view('includes/template',$data);
		}

		function update_mark() {
			$data = $this->get_data();
			// $song_id = $this->uri->segment(3);
			// $user_id = $this->session->userdata('user_id');
			$new_mark = $this->input->post('new_mark');

			$this->ml->m_update_mark($new_mark, $data['user_id'], $this->song_id);

			//$new_rate = ///
			/*------------------------------------------------
			| comment
			------------------------------------------------*/
			$this->ml->m_update_rate($new_rate, $this->song_id);

			$data['new_rate'] = $new_rate;
			/*------------------------------------------------
			| AJAX
			------------------------------------------------*/
			$this->load->view($data) ;
		}
		
		function add_comment() {
			$data = $this->get_data();
			// $user_id = $this->session->userdata('user_id');
			// $song_id = $this->uri->segment(3);
			$comment = $this->input->post('comment');

			$this->ml->m_add_comment($comment, $data['user_id'], $thid->song_id);
			
			$data['response'] = 'Your comment successfully added!';
			$this->load->view($data);
		}
	}