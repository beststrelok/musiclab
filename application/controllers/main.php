<?php 
	class Main extends MY_Controller {

		function index() {
			$data = $this->get_data();
			$data['songs'] = $this->ml->m_get_songs();

			$data['main_content'] = 'v_main';

			$this->load->view('includes/template',$data);
		}

		function search() {
			$param = $this->input->post('search');

			$data = $this->get_data();
			$data['songs'] = $this->ml->m_search($param);
			$data['main_content'] = 'v_main';

			$this->load->view('includes/template',$data);
		}
	}