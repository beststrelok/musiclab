<?php 
	class Profile extends MY_Controller {

		// function __construct() {
		// 	parent::__construct();
		// 	/*------------------------------------------------
		// 	| Checking for login
		// 	------------------------------------------------*/
		// 	$is_logged_in = $this->session->userdata('is_logged_in');
		// 	if ($is_logged_in !== 'YES') {
		// 		redirect('welcome');
		// 	} else {
		// 		$this->current_user_id = $this->session->userdata('user_id');
		// 	}
		// }

		function index() {
			$data = $this->get_data();
			$data['user'] = $this->ml->m_get_user($data['user_id']);
			$data['rated_songs'] = $this->ml->m_get_rated($data['user_id']);
			
			$data['main_content']='v_profile';
			$this->load->view('includes/template', $data);
		}		


		function update_profile() {
			$new_creds = array(
				'login'			=> $this->input->post('login'),
				'password'		=> md5($this->input->post('password')),
				'first_name'	=> $this->input->post('first_name'),
				'last_name'		=> $this->input->post('last_name'),
				'email'			=> $this->input->post('email'),
				'avatar'		=> NULL,
				'nickname'		=> $this->input->post('nickname')
			);

			$user_id = $this->session->userdata('user_id');			$user_id = $this->session->userdata('user_id');
			$this->ml->m_update_creds($new_creds, $user_id);
			$data = $this->get_data();

			$data['response'] = 'Your profile successfully updated!';
			$this->load->view($data);
		}
	}