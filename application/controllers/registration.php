<?php 
	class Registration extends MY_Controller {

		function index() {
			$data = $this->get_data();
			$data['main_content']='v_registration';
			$this->load->view('includes/template', $data);			
		}

		function create_user() {
			$this->form_validation->set_rules('first_name', 'Имя', 'trim|required|min_length[4]|max_length[32]');
			$this->form_validation->set_rules('last_name', 'Фамилия', 'trim|required|min_length[4]|max_length[32]');
			$this->form_validation->set_rules('nickname', 'Ник', 'trim|required|min_length[4]|max_length[32]|is_unique[users.nickname]');
			$this->form_validation->set_rules('login', 'Логин', 'trim|required|is_unique[users.login]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');	
			$this->form_validation->set_rules('password', 'Пароль', 'trim|required|min_length[6]|max_length[32]');
			$this->form_validation->set_rules('passconf', 'Подтверждение', 'trim|required|matches[password]');

			if ($this->form_validation->run() === FALSE) {
				$this->index();
			} else {

				$new_user = array(
					'login'			=> $this->input->post('login'),
					'password'		=> md5($this->input->post('password')),
					'first_name'	=> $this->input->post('first_name'),
					'last_name'		=> $this->input->post('last_name'),
					'email'			=> $this->input->post('email'),
					'nickname'		=> $this->input->post('nickname'),
				);

				$is_inserted = $this->ml->m_create_user($new_user);

				if ($is_inserted) {
					/*------------------------------------------------
					| Sending Email
					------------------------------------------------*/
					// $this->email->from('admin@resume.com', 'Your Name');
					// $this->email->to($this->input->post('email_address')); 
					// $this->email->subject('Регистрация');
					// $this->email->message("Ваши регистрационные данные: Логин - ".$this->input->post('login_name').', Пароль - '.$this->input->post('password'));	

					// $this->email->send();

					//echo $this->email->print_debugger();
					/*----------------------------------------------*/
					$data = $this->get_data();
					$data['main_content']='v_success';
					$this->load->view('includes/template', $data);
				} else {
					redirect('registration');
				}
			}
		}
	}