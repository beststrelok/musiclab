<?php 
	class Api extends MY_Controller {

		function login() {

			$login_log = $this->input->post('login_log');
			$password_log = md5($this->input->post('password_log'));
			$valid_user = $this->ml->m_val_creds($login_log, $password_log);

			$ajax = new stdClass();

			if ($valid_user) {
				$ajax->login = TRUE; 
				$ajax->nickname = $valid_user->nickname;
				$ajax->avatar = $valid_user->avatar; 
				$ajax->user_id = $valid_user->user_id;

				$data2 = array(
					'user_id'		=> $valid_user->user_id,
					'nickname'		=> $valid_user->nickname,
					'avatar'		=> $valid_user->avatar,
					'is_logged_in'	=> 'YES'
				);

				$this->session->set_userdata($data2);
			} else {
				$ajax->login = FALSE; 
			}
			echo json_encode($ajax);
		}

		function logout() {
			$ajax = new stdClass();
			$this->session->sess_destroy();

			$ajax->login = FALSE;
			echo json_encode($ajax);
		}

		function get_marks() {
			$ajax = new stdClass();
			$data = $this->get_data();

			$marks = $this->ml->m_get_marks($data['user_id']);

			$ajax->marks = $marks;
			$ajax->user_id = $data['user_id'];
			echo json_encode($ajax);
		}

		function update_mark_rate() {
			$ajax = new stdClass();
			$data = $this->get_data();

			$new_mark = $this->input->post('new_mark');
			$song_id = $this->input->post('song_id');
			
			/*------------------------------------------------
			| Update mark
			------------------------------------------------*/
			$this->ml->m_update_mark($new_mark, $data['user_id'], $song_id);

			/*------------------------------------------------
			| Update rate
			------------------------------------------------*/
			$all_marks = $this->ml->m_all_marks($song_id);

			$summ = 0;
			foreach ($all_marks as $song) {
				foreach ($song as $key => $mark) {
					if ($key == 'mark') {
						$summ += $mark;
					}
				}
			}
			$length = count($all_marks);

			$new_rate = $summ/$length;

			$this->ml->m_update_rate($new_rate, $song_id);
			

			$ajax->all_marks = $all_marks;
			$ajax->$new_rate = $new_rate;
			echo json_encode($ajax);
		}
	}