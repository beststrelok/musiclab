<?php 
	class Ml extends CI_Model {

/*------------------------------------------------
| Songs
------------------------------------------------*/
		function m_get_songs($song_id = NULL) {
			if ($song_id) {
				$this->db->where('song_id', $song_id);
				$q = $this->db->get('songs');
				if ($q->num_rows() > 0) {
					$songs = $q->result();
					return $songs;
				}
			} else {
				$this->db->order_by('rate', 'desc');
				$q = $this->db->get('songs');
				if ($q->num_rows() > 0) {
					$songs = $q->result();
					return $songs;
				}
			}
		}

		function m_get_rated($user_id) {
			$this->db->where('user_id', $user_id);
			$this->db->select('song_id');
			$song_ids = $this->db->get('ratings');
			print_r($song_ids);
			/*------------------------------------------------
			| undone
			------------------------------------------------*/
		} 

		function m_update_rate($new_rate, $song_id) {
			$this->db->where('song_id', $song_id);
			
			$data = array(
				'rate'	=> $new_rate
			);

			$this->db->update('songs', $data); 
		}

/*------------------------------------------------
| Ratings
------------------------------------------------*/
		function m_update_mark($new_mark, $user_id, $song_id, $comment = NULL) {
			$time = date("Y-m-d H:i:s"); 

			$data = array(
               'mark' 		=> $new_mark,
               'time' 		=> $time,
               'user_id' 	=> $user_id,
               'song_id' 	=> $song_id
            );

			$this->db->where('user_id', $user_id);
			$this->db->where('song_id', $song_id);
			$query = $this->db->get('ratings');

			if ($query->num_rows() > 0) {
				$this->db->where('user_id', $user_id);
				$this->db->where('song_id', $song_id);
				$this->db->update('ratings', $data);
			} else {
				$this->db->where('user_id', $user_id);
				$this->db->where('song_id', $song_id);
				$this->db->insert('ratings', $data);
			}
		}

		function m_best_mark($look) {
			$this->db->where('user_id', $look);
			$q = $this->db->get('ratings');		// sort by DESC
			if ($q->num_rows() > 0) {
				$popular = $q->result();
				return $popular;
			}
		}

		function m_get_marks($user_id) {
			// $this->db->from('ratings');
			// $this->db->join('songs', 'songs.song_id = ratings.song_id', 'left');
			// $this->db->order_by('rate', 'desc');
			$this->db->where('user_id', $user_id);
			$this->db->select('song_id, mark');
			$data = $this->db->get('ratings');
			$data = $data->result_array();
			return $data;
		}

		function m_all_marks($song_id) {
			$this->db->where('song_id', $song_id);
			$this->db->select('mark');
			$data = $this->db->get('ratings');
			$data = $data->result_array();
			return $data;
		}

		function m_get_comments($song_id) {
			$this->db->where('song_id', $song_id);
			$q = $this->db->get('ratings');
			if ($q->num_rows() > 0) {
				$comments = $q->result();
				// print_r($comments);
				return $comments;
			}
		}

		function m_last_comments($user_id) {
			$this->db->where('user_id', $user_id);
			$q = $this->db->get('ratings');
			if ($q->num_rows() > 0) {
				$last_comments = $q->result();
				// print_r($last_comments);
				return $last_comments;
			}
		}

		function m_add_comment($comment, $user_id, $song_id) {
			$this->db->where('song_id', $song_id);
			$this->db->where('user_id', $user_id);			
			$this->db->set('comment', $comment); 
			$this->db->insert('ratings'); 
		}

/*------------------------------------------------
| Users
------------------------------------------------*/
		function m_create_user($new_user) {
			return $this->db->insert('users', $new_user); 
		}

		function m_get_user($user_id) {
			$this->db->where('user_id', $user_id);
			$q = $this->db->get('users');
			if ($q->num_rows() > 0) {
				$user = $q->result();
				// print_r($user[0]);
				return $user[0];
			}
		}

		function m_update_creds($new_creds, $user_id) {
			$this->db->where('user_id', $user_id);
			$this->db->update('users', $new_creds); 
		}
/*------------------------------------------------
| Search
------------------------------------------------*/
		function m_search($param = NULL) {
			$this->db->like('title', $param);
			$this->db->order_by('rate', 'desc');
			$titles = $this->db->get('songs');

			$this->db->like('nickname', $param);
			$nicknames = $this->db->get('users');

			$this->db->like('artist', $param);
			$this->db->order_by('rate', 'desc');
			$artists = $this->db->get('songs');

			if (($titles->num_rows() > 0) || ($nicknames->num_rows() > 0) || ($artists->num_rows() > 0)) {
				// $results = array(
				// 'search_titles	'	 => $titles->result(),
				// 	'search_nicknames' 	 => $nicknames->result(),
				// 	'search_artists	'	 => $artists->result()
				// );
				// $results = array();
				// array_push($results, $titles->result());
				// array_push($results, $articles->result());
				// array_push($results, $nicknames->result());

				// print_r($results);
				return $titles->result();
			}
		}

		function m_val_creds($login_log, $password_log) {
			$this->db->where('login', $login_log);
			$this->db->where('password', $password_log);
			$valid_user = $this->db->get('users');
			$valid_user = $valid_user->result();
			if ($valid_user) {
				return $valid_user[0];
			} 			
		}
	}