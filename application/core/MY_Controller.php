<?php 
	class MY_Controller extends CI_Controller {
		function __construct() {
        	parent::__construct();
    	}

    	function valid() {
			$is_logged_in = $this->session->userdata('is_logged_in');
			if ($is_logged_in === 'YES') {
				$login = TRUE; 
			} else {
				$login = FALSE;
			}
			return $login;
		}

		function get_data() {
			$data['login'] = $this->valid();
			$data['user_id'] = $this->session->userdata('user_id');
			$data['avatar'] = $this->session->userdata('avatar');
			$data['nickname'] = $this->session->userdata('nickname');
			return $data;
		}
	}