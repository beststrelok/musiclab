      <?php if (isset($status)) : ?>
        <div class="wrong_creds">
          <?php echo $status; ?>
        </div>
      <?php endif; ?>


      <div class="content">
        <?php if($songs) : ?>

          <?php foreach ($songs as $song) : ?>
            <div class="song" data-song_id="<?php echo $song->song_id; ?>">
              <p class='rate'># <?php echo $song->rate; ?></p>
              <a href="<?php echo base_url("song/index/$song->song_id"); ?>">
                <img src="<?php echo base_url("img/$song->picture"); ?>.jpg" alt="">
                <p class='desc'><?php echo $song->artist; ?> - <?php echo $song->title; ?></p>
              </a>

              <div class="input select rating-f">
              <select id="example-f" name="rating" style="display: none;">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
              </div>
            
            </div>
          <?php endforeach; ?>
        <?php else : ?>
          <h4 class='not_found'>No elements found.</h4>
        <?php endif; ?> <!-- endif songs -->
     </div>
