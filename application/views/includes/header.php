<script>
	/*------------------------------------------------
	| SETTING GLOBAL VARS
	------------------------------------------------*/
    var AJAX_ADDRESS = "<?php echo base_url(); ?>";
    var LOGIN = "<?php echo $login; ?>";  // '1' or ''
   	var USER_ID = "<?php echo $user_id; ?>";
   	var NICKNAME = "<?php echo $nickname; ?>";
   	var AVATAR = "<?php echo $avatar; ?>";
    /*----------------------------------------------*/
</script>



<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>MusicLab</title>
		<link type='text/css' rel="stylesheet" href='<?php echo base_url("css/style.css"); ?>'>
		<link type='text/css' rel="stylesheet" href='<?php echo base_url("css/bootstrap.css"); ?>'>
		<script charset='UTF-8' type='text/javascript' src='<?php echo base_url("js/jquery-2.0.3.js"); ?>'></script>
		<script charset='UTF-8' type='text/javascript' src='<?php echo base_url("js/bootstrap.js"); ?>'></script>
		<script charset='UTF-8' type='text/javascript' src='<?php echo base_url("js/barrating.js"); ?>'></script>
	</head>
	<body>


	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    	<div class="container">
        	<div class="navbar-header">
	        	<?php echo anchor('main','MusicLab', array('class'=>'navbar-brand')); ?>
        	</div>

	        <div class="collapse navbar-collapse">
	          	<?php echo form_open('main/search', 'class="navbar-form navbar-left"'); ?>
		            <div class="form-group">
						<input type="text" name='search' class="form-control rounded" placeholder="Looking for smth?">
		            </div>
		            <span class="glyphicon glyphicon-search"></span>            
		            <button type="submit" class="btn btn-success">Search</button>
	         	<?php echo form_close(); ?>
	        </div>

	        <form class="navbar-form navbar-right">
	        	<div id="logged_in">
			        <div class="form-group">
			          	<input type="text" placeholder="Login" class="form-control" name='login_log' id='login_log'>
			        </div>
			        <div class="form-group">
			            <input type="password" placeholder="Password" class="form-control" name='password_log' id='password_log'> 
			        </div>
			        <a class='btn btn-success' id='login_button'>Log In</a>
			        <?php echo anchor('registration','Registration', array('class'=>'btn btn-primary')); ?>
			    </div>
				
				<div id="logged_out">
			        <a class='btn btn-primary' id='logout_button'>Log Out</a>
			    </div>
				<div class="personal">
					<a href="<?php echo base_url('user/index'); ?>/<?php echo $user_id; ?>">
						<p class="nickname"></p>	
						<img class='avatar' src=""/>
					</a>
				</div>
			</form>
	      </div>
    </div>
