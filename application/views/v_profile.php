<div class="menu">
	<?php 
		echo anchor('welcome','Главная',array('class'=>'welcome button'));
		echo anchor('contact','Контакты',array('class'=>'contact button'));	
		echo anchor('office','Личный Кабинет',array('class'=>'office button'));
		echo anchor('main/logout','Выйти',array('class'=>'logout button last'));
		echo '<p class="user last">'.$user_data->first_name.' '.substr($user_data->last_name, 0, 2).'.</p>';
	?>
	<div class="icon last"><img src="<?php echo base_url(); ?>img/thumbs/<?php echo $resume_data->logo; ?>.jpg" alt=""></div>
</div>

<div class="resume">

	<?php echo form_open('office/change_resume'); ?>

		<h2 class='res'>Личный Кабинет</h2>
		<?php echo form_submit('submit','Сохранить'); ?>
		<?php echo anchor('office/delete_resume','Удалить Резюме', array('class'=>'del_res')); ?>

		<h3><?php echo $user_data->last_name.' '.$user_data->first_name; ?></h3>


		<table class='personal'>
			<tr>
				<td>Дата рождения:</td>
				<td><?php echo $user_data->birthday; ?></td>
			</tr>
			<tr>
				<td>Телефон:</td>
				<td><?php echo $user_data->mobile; ?></td>
			</tr>
			<tr>
				<td>E-mail:</td>
				<td><?php echo $user_data->email_address; ?></td>
			</tr>
		</table>
		<div class="picture">
			<img src="<?php echo base_url(); ?>img/<?php echo $resume_data->picture; ?>.jpg" alt="">
			<?php echo anchor('office/upload','изменить', array('class'=>'span')); ?>
		</div>
		

		<table class='add'>
			<tr>
				<td>Специальность:</td>
				<td><?php echo form_input('title', $resume_data->title); ?></td>
			</tr>
			<tr>
				<td>Желаемая з/п:</td>
				<td><?php echo form_input('salary', $resume_data->salary); ?></td>
			</tr>
			<tr>
				<td>Цель:</td>
				<td><?php echo form_textarea('aim', $resume_data->aim); ?></td>
			</tr>
			<tr>
				<td>Профессиональные навыки:</td>
				<td><?php echo form_textarea('prof_info', $resume_data->prof_info); ?></td>
			</tr>
			<tr>
				<td>Образование:</td>
				<td><?php echo form_textarea('education', $resume_data->education); ?></td>
			</tr>
			<tr>
				<td>Владение языками:</td>
				<td><?php echo form_textarea('languages', $resume_data->languages); ?></td>
			</tr>
			<tr>
				<td>Прочее:</td>
				<td><?php echo form_textarea('other', $resume_data->other); ?></td>
			</tr>
		</table>

	<?php echo form_close(); ?>
</div>		