   <div class="v_registration">
    <div class="container">

      <?php echo form_open('registration/create_user', 'class="form-signin"'); ?>
        <h2 class="form-signin-heading">Registration Form</h2>
        <input type="text" class="form-control" placeholder="First Name" name='first_name' required autofocus>
        <input type="text" class="form-control" placeholder="Last Name" name='last_name' required autofocus>
        <input type="text" class="form-control" placeholder="Login" name='login' required autofocus>
        <input type="text" class="form-control" placeholder="Nickname" name='nickname' required autofocus>
        <input type="text" class="form-control" placeholder="Email Address" name='email' required autofocus>
        <input type="password" class="form-control" placeholder="Password" name='password' required autofocus>
        <input type="password" class="form-control" placeholder="Password Confirmation" name='passconf' required autofocus>
        <button type='submit' class="btn btn-lg btn-primary btn-block">Sign Up</button>
        <?php echo anchor('main','Back To MusicLab', array('class'=>'btn btn-lg btn-primary btn-block')); ?>
        <?php echo validation_errors('<div class="error">', '</div>'); ?>
      <?php echo form_close(); ?>

    </div> <!-- /container -->
  </div>
