<div class="v_success">
	<h1>Congratulations!</h1>
	<div class="congrat">
		<p>Your registration request is accepted.</p>
        <?php echo anchor('main','Back To MusicLab', array('class'=>'btn btn-primary')); ?>
	</div>
</div>