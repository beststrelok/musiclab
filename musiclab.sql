-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 13 2013 г., 10:23
-- Версия сервера: 5.5.34-0ubuntu0.13.04.1
-- Версия PHP: 5.4.9-4ubuntu2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `musiclab`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `mark_id` int(11) NOT NULL AUTO_INCREMENT,
  `mark` int(3) NOT NULL,
  `comment` text NOT NULL,
  `time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  PRIMARY KEY (`mark_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=212 ;

--
-- Дамп данных таблицы `ratings`
--

INSERT INTO `ratings` (`mark_id`, `mark`, `comment`, `time`, `user_id`, `song_id`) VALUES
(188, 5, '', '2013-12-13 10:19:23', 1, 1),
(189, 5, '', '2013-12-13 10:19:30', 1, 3),
(190, 5, '', '2013-12-13 10:19:33', 1, 7),
(191, 1, '', '2013-12-13 10:19:36', 1, 2),
(192, 1, '', '2013-12-13 10:19:37', 1, 4),
(193, 1, '', '2013-12-13 10:19:39', 1, 8),
(194, 1, '', '2013-12-13 10:19:40', 1, 6),
(195, 1, '', '2013-12-13 10:19:41', 1, 5),
(196, 4, '', '2013-12-13 10:20:07', 2, 1),
(197, 4, '', '2013-12-13 10:20:08', 2, 3),
(198, 4, '', '2013-12-13 10:20:09', 2, 7),
(199, 2, '', '2013-12-13 10:20:11', 2, 2),
(200, 2, '', '2013-12-13 10:20:13', 2, 4),
(201, 3, '', '2013-12-13 10:20:14', 2, 5),
(202, 1, '', '2013-12-13 10:20:14', 2, 6),
(203, 5, '', '2013-12-13 10:20:16', 2, 8),
(204, 2, '', '2013-12-13 10:20:45', 3, 1),
(205, 2, '', '2013-12-13 10:22:07', 3, 3),
(206, 3, '', '2013-12-13 10:20:47', 3, 7),
(207, 5, '', '2013-12-13 10:20:49', 3, 8),
(208, 5, '', '2013-12-13 10:20:51', 3, 6),
(209, 3, '', '2013-12-13 10:20:54', 3, 5),
(210, 3, '', '2013-12-13 10:20:55', 3, 2),
(211, 3, '', '2013-12-13 10:21:12', 3, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `songs`
--

CREATE TABLE IF NOT EXISTS `songs` (
  `song_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `artist` varchar(32) NOT NULL,
  `album` varchar(32) NOT NULL,
  `picture` varchar(64) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `link` varchar(128) NOT NULL,
  PRIMARY KEY (`song_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `songs`
--

INSERT INTO `songs` (`song_id`, `title`, `artist`, `album`, `picture`, `rate`, `link`) VALUES
(1, 'Roar', 'Katy Perry', 'Prism', 'roar', 3.67, 'song_link_goes_here'),
(2, 'Undressed', 'Kim Cesarion', '', 'undressed', 2.00, 'song_link_goes_here'),
(3, 'Royals', 'Lorde', 'The Love Club EP', 'royals', 3.67, 'song_link_goes_here'),
(4, 'Sous L''oeil De L''ange', 'K-Maro', 'La Good Life', 'k-maro', 2.00, 'song_link_goes_here'),
(5, 'Profites', 'Costello', 'Prism', 'profites', 2.33, 'song_link_goes_here'),
(6, 'Empire Stete of Mind', 'Jay-Z', 'The Blueprint 3', 'empire_state', 2.33, 'song_link_goes_here'),
(7, 'Cheers', 'Rihanna', 'Loud', 'cheers', 4.00, 'song_link_goes_here'),
(8, 'A Milli', 'Lil Wayne', 'The Carter 3', 'milli', 3.67, 'song_link_goes_here');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `first_name` varchar(16) NOT NULL,
  `last_name` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `avatar` varchar(64) NOT NULL,
  `nickname` varchar(16) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `login`, `password`, `first_name`, `last_name`, `email`, `avatar`, `nickname`) VALUES
(1, 'yulia', '03be66295cd7eb6cf6001c9181bb904d', 'Юлия', 'Тимошенко', 'yulia@gmail.com', 'timoshenko', 'timoshenko'),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin', 'admin@gmail.com', 'stallone', 'admin'),
(3, 'obama', 'a40c0ee90451a127cc66eef339c378cd', 'obama', 'obama', 'obama@gmail.com', 'obama', 'obama'),
(4, 'qweqwe', 'efe6398127928f1b2e9ef3207fb82663', 'qweqwe', 'qweqwe', 'qweqwe@gmail.com', '', 'qweqwe'),
(5, 'newone', '08b1d443ef0ab3677d2af8ef1afb1b28', 'newone', 'newone', 'newone@gmail.com', '', 'newone'),
(6, 'newtwo', 'c49b7cff7f50df183394de213acecec0', 'newtwo', 'newtwo', 'newtwo@gmail.com', '', 'newtwo'),
(7, 'newthree', 'f32866335231a089ad9dda79efeb9169', 'newthree', 'newthree', 'newthree@gmail.com', '', 'newthree'),
(8, 'fourone', '3afc2ad002a921d4a13720143f7904dd', 'fourone', 'fourone', 'fourone@gmail.com', '', 'fourone'),
(9, 'fiveone', 'a0d88ddbf513c846c8bbb23492ea71ec', 'fiveone', 'fiveone', 'fiveone@gmail.com', '', 'fiveone'),
(10, 'fiveone1', '98334cdc2197e9f286ed99039131c743', 'fiveone1', 'fiveone1', 'fiveone1@gmail.com', '', 'fiveone1'),
(11, 'fiveone2', '05e8a291fb13b7265b80ed8bf4e7da35', 'fiveone2', 'fiveone2', 'fiveone2@gmail.com', '', 'fiveone2'),
(12, 'fiveone3', 'ba9f121c6ea5ad55e91bd79e5b2b73ab', 'fiveone3', 'fiveone3', 'fiveone3@gmail.com', '', 'fiveone3'),
(13, 'fiveone4', '2df7f3d0b98842e160ab74ab2d125cdc', 'fiveone4', 'fiveone4', 'fiveone4@gmail.com', '', 'fiveone4'),
(14, 'sixone', '3d5c2be2a599a3aab4c8a4398fdc023c', 'sixone', 'sixone', 'sixone@gmail.com', '', 'sixone'),
(15, 'sevenone', '255b77f51405208ab418e8df4a4457db', 'sevenone', 'sevenone', 'sevenone@gmail.com', '', 'sevenone'),
(16, 'eightone', '909f039dff176406cb15381e1889c2d3', 'eightone', 'eightone', 'eightone@gmail.com', '', 'eightone'),
(17, 'denwer', 'ba2ba2bfa3b59cfd77d74f43e915a9ac', 'denwer', 'denwer', 'denwer@gmail.com', '', 'denwer'),
(18, 'denwer2', 'c0a6c6e7912d25dd3478a687c3ea1fb4', 'denwer2', 'denwer2', 'denwer2@gmail.com', '', 'denwer2'),
(19, 'denwer3', '03fff8fec35d5db2b0d92492b2ea33e4', 'denwer3', 'denwer3', 'denwer3@gmail.com', '', 'denwer3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
