(function ($) {
var SPEED = 300;
$('select').barrating('show');

/*------------------------------------------------
| GET MARKS OF CURRENT USER
------------------------------------------------*/
function get_marks() {
	$.ajax({
	 	url: AJAX_ADDRESS+'api/get_marks',
	 	type: 'POST',
	 	dataType: "json",
	 	success: get_marks_success, 
	   	error: function(data, error, error_details){
	   		console.log("err:",error, error_details);
	 	   	console.log(data);
	   	}
	});	
};

function get_marks_success(response) { 
	var marks = response.marks;

	/*------------------------------------------------
	| Delete old marks at first
	------------------------------------------------*/
	$('div.song').find('.br-widget a')
		.removeClass('br-current')
			.removeClass('br-selected');

	/*------------------------------------------------
	| When it were marks
	------------------------------------------------*/
	for (i=0; i<marks.length;i++) {
		var song_id = marks[i].song_id;
		var mark = marks[i].mark;

		var $song = $('div.song').filter(function(ind) {
			return $(this).data('song_id') == song_id; // USE == !!!
			})

		$song.find('.br-widget a')
			.eq(mark-1)
				.addClass('br-current');

		$song.find('.br-widget a:lt('+mark+')')
			.addClass('br-selected');
	};
	/*------------------------------------------------
	| When it were no marks
	------------------------------------------------*/
	if (marks.length == 0) {
		$('div.song')
			.find('.br-widget a')
				.removeClass('br-current')
					.removeClass('br-selected');
	};
};
/*------------------------------------------------
| CHANGE MARK AND RATE
------------------------------------------------*/
	$('.br-widget a').on('click', function(evt) {
		var new_mark = $(this).data('rating-value');
		var song_id = $(this).closest('.song').data('song_id');
		var user_id = USER_ID; // changed with ajax

		console.log('new_mark: '+new_mark);
		console.log('song_id: '+song_id);
		console.log('user_id: '+user_id);

		var data = {
			'new_mark'	: new_mark,
			'song_id'	: song_id,
			'user_id'	: user_id
		};

	$.ajax({
     	url: AJAX_ADDRESS+'api/update_mark_rate',
     	type: 'POST',
     	data: data,
     	dataType: "json",
     	success: function(response) { 
     		console.log(response);
 	   	},
 	   	error: function(data, error, error_details){
 	   		console.log("err:",error, error_details);
 	   		console.log(data);
 	   	}
	});
});
/*------------------------------------------------
| PAGE REFRESH
------------------------------------------------*/
if (LOGIN) {

	$('#logged_out').show();
	$('#logged_in').hide();
	$('.br-widget').show();
	$('.personal').show();
	$('p.nickname').html('Hi, '+NICKNAME+'!');

	/*------------------------------------------------
	| set avatar
	------------------------------------------------*/
	if (AVATAR != '') {
		var src = AVATAR;
	} else {
		var src = 'empty_default_picture';
	}
	$('img.avatar').attr('src',AJAX_ADDRESS+'img/avatars/thumbs/'+src+'.jpg');

	get_marks(); // Get all marks of current user if LOGGED_IN

} else {
	$('#logged_out').hide();
	$('#logged_in').show();
	$('.br-widget').hide();
	$('.personal').hide();
};
/*------------------------------------------------
| LOGIN
------------------------------------------------*/
$('#login_button').on('click', function() {
	var login_log = $('#login_log').val();		
	var password_log = $('#password_log').val();	

	var creds = {
		'login_log'		: login_log,
		'password_log'	: password_log 
	};

	$.ajax({
     	url: AJAX_ADDRESS+'api/login',
     	type: 'POST',
     	data: creds,
     	dataType: "json",
     	success: login_success,
 	   	error: function(data, error, error_details){
 	   		console.log("err:",error, error_details);
 	   		console.log(data);
 	   	}
	});
});

function login_success(response) { 
	if(response.login) {
		$('#logged_out').delay(SPEED).fadeIn(SPEED);
		$('#logged_in').fadeOut(SPEED);
		$('.br-widget').slideDown(SPEED*2);
		$('.personal').delay(SPEED).fadeIn(SPEED);

		$('p.nickname').html('Hi, '+response.nickname+'!');

		/*------------------------------------------------
		| set avatar
		------------------------------------------------*/
		if (response.avatar != '') {
			var src = response.avatar;
		} else {
			var src = 'empty_default_picture';
		};

		$('img.avatar').attr('src',AJAX_ADDRESS+'img/avatars/thumbs/'+src+'.jpg');

		/*------------------------------------------------
		| Change current user
		------------------------------------------------*/
		USER_ID = response.user_id;
		get_marks()

		/*------------------------------------------------
		| Redirect if user logged in at registration page
		------------------------------------------------*/
		if (location.pathname == '/musiclab/registration') {
			setTimeout(function() {
				location.pathname = '/musiclab';
			}, SPEED*2);
		};
	};
};
/*------------------------------------------------
| LOGOUT
------------------------------------------------*/
$('#logout_button').on('click', function() {
	$.ajax({
     	url: AJAX_ADDRESS+'api/logout',
     	dataType: "json",
     	success: logout_success,
 	   	error: function(data, error, error_details){
 	   		console.log("err:",error, error_details);
 	   		console.log(data);
 	   	}
		});
});

function logout_success(response) { 
	if( ! response.login) {
		$('#logged_out').fadeOut(SPEED);
		$('#logged_in').delay(SPEED).fadeIn(SPEED);
		$('.br-widget').slideUp(SPEED*2);
		$('.personal').fadeOut(SPEED);
	};
};
/*----------------------------------------------*/
})(jQuery);


/*------------------------------------------------
| TRIGGERS
------------------------------------------------*/
	// $(document).on('api/get_marks', function(evt) {
	// 	console.log(evt);
	// 	$.ajax({
	// 	 	url: AJAX_ADDRESS+'api/get_marks',
	// 	 	dataType: "json",
	// 	 	success: function(response) { 
	// 	    	console.log(response);
	// 		},
	// 	   	error: function(data, error, error_details){
	// 	   		console.log("err:",error, error_details);
	// 	 	   	console.log(data);
	// 	   	}
	// 	});	
	// });
/*------------------------------------------------
| use
------------------------------------------------*/
	// $(document).trigger('api/get_marks');
/*----------------------------------------------*/